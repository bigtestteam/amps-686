<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.atlassian.amps</groupId>
        <artifactId>atlassian-amps-parent</artifactId>
        <version>6.3.19-SNAPSHOT</version>
    </parent>

    <groupId>com.atlassian.maven.plugins</groupId>
    <artifactId>maven-amps-plugin</artifactId>
    <packaging>maven-plugin</packaging>

    <name>Maven AMPS Plugin</name>

    <dependencies>
        <dependency>
            <groupId>postgresql</groupId>
            <artifactId>postgresql</artifactId>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
        </dependency>
        <dependency>
            <groupId>com.oracle</groupId>
            <artifactId>ojdbc6</artifactId>
        </dependency>
        <dependency>
            <groupId>net.sourceforge.jtds</groupId>
            <artifactId>jtds</artifactId>
        </dependency>
        <dependency>
            <groupId>com.microsoft.sqlserver</groupId>
            <artifactId>mssql-jdbc</artifactId>
        </dependency>
        <dependency>
            <groupId>com.atlassian.core</groupId>
            <artifactId>atlassian-core</artifactId>
            <version>4.0</version>
        </dependency>
        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>2.4</version>
        </dependency>
        <dependency>
            <groupId>biz.aQute.bnd</groupId>
            <artifactId>biz.aQute.bndlib</artifactId>
            <version>2.4.1-pr-916-atlassian-002</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.ant</groupId>
            <artifactId>ant</artifactId>
            <version>1.7.1</version>
        </dependency>
        <dependency>
            <groupId>org.apache.maven</groupId>
            <artifactId>maven-core</artifactId>
            <version>${bundled.maven.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.maven</groupId>
            <artifactId>maven-model</artifactId>
            <version>${bundled.maven.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.maven</groupId>
            <artifactId>maven-compat</artifactId>
            <version>${bundled.maven.version}</version>
        </dependency>
        <dependency>
            <groupId>org.codehaus.plexus</groupId>
            <artifactId>plexus-utils</artifactId>
            <!-- Version required by mojo-executor -->
            <version>3.0.15</version>
        </dependency>

        <!-- Depend on libraries used, rather than on plugins -->
        <dependency>
            <groupId>org.codehaus.plexus</groupId>
            <artifactId>plexus-archiver</artifactId>
            <version>1.0-alpha-9</version>
        </dependency>
        <dependency>
            <groupId>org.apache.maven</groupId>
            <artifactId>maven-archiver</artifactId>
            <version>2.3</version>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-compress</artifactId>
            <version>1.4.1</version>
        </dependency>
        <dependency>
            <groupId>commons-validator</groupId>
            <artifactId>commons-validator</artifactId>
            <version>1.2.0</version>
        </dependency>

        <!-- needed for plugin module generation -->
        <dependency>
            <groupId>jdom</groupId>
            <artifactId>jdom</artifactId>
            <version>1.0</version>
        </dependency>
        <dependency>
            <groupId>com.atlassian.amps</groupId>
            <artifactId>plugin-module-codegen-maven-client</artifactId>
            <version>${project.version}</version>
        </dependency>

        <!-- refapp archetype for integration testing -->
        <dependency>
            <groupId>com.atlassian.maven.archetypes</groupId>
            <artifactId>refapp-plugin-archetype</artifactId>
            <version>${project.version}</version>
            <scope>runtime</scope>
        </dependency>

        <!-- google tracking -->
        <dependency>
          <groupId>com.dmurph</groupId>
          <artifactId>JGoogleAnalyticsTracker</artifactId>
          <version>1.2.1-atlassian</version>
        </dependency>
        <dependency>
          <groupId>org.slf4j</groupId>
          <artifactId>slf4j-nop</artifactId>
          <version>1.6.2</version>
        </dependency>

        <dependency>
            <groupId>org.codehaus.jackson</groupId>
            <artifactId>jackson-mapper-asl</artifactId>
            <version>1.9.13</version>
        </dependency>

        <dependency>
          <groupId>com.atlassian.amps</groupId>
          <artifactId>standalone</artifactId>
          <version>${project.version}</version>
          <type>pom</type>
          <scope>runtime</scope>
        </dependency>

        <!-- func test framework -->
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-osgi-testrunner</artifactId>
            <version>${plugin.testrunner.version}</version>
        </dependency>
        <dependency>
            <groupId>javax.ws.rs</groupId>
            <artifactId>jsr311-api</artifactId>
            <version>1.1.1</version>
        </dependency>


        <!-- minification -->
        <dependency>
            <groupId>com.google.javascript</groupId>
            <artifactId>closure-compiler</artifactId>
            <version>rr2079.1</version>
        </dependency>
        <dependency>
            <groupId>com.yahoo.platform.yui</groupId>
            <artifactId>yuicompressor</artifactId>
            <version>2.4.7</version>
        </dependency>
        <dependency>
            <groupId>commons-codec</groupId>
            <artifactId>commons-codec</artifactId>
            <version>1.6</version>
        </dependency>
        <dependency>
            <groupId>com.googlecode.htmlcompressor</groupId>
            <artifactId>htmlcompressor</artifactId>
            <version>1.4</version>
        </dependency>
        <dependency>
            <groupId>org.apache.wink</groupId>
            <artifactId>wink-client-apache-httpclient</artifactId>
            <version>1.1.3-incubating</version>
        </dependency>

        <!-- Only used in tests -->
        <dependency>
            <groupId>com.googlecode.guava-osgi</groupId>
            <artifactId>guava-osgi</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.apache.maven.archetype</groupId>
            <artifactId>archetype-common</artifactId>
            <version>2.2</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-shade-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>shade</goal>
                        </goals>
                        <configuration>
                            <createDependencyReducedPom>true</createDependencyReducedPom>
                            <artifactSet>
                                <includes>
                                    <include>org.twdata.maven:mojo-executor</include>
                                </includes>
                            </artifactSet>
                            <transformers>
                                <transformer implementation="org.apache.maven.plugins.shade.resource.ComponentsXmlResourceTransformer" />
                            </transformers>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-assembly-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>attached</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <outputDirectory>${project.build.directory}</outputDirectory>
                    <descriptors>
                        <descriptor>src/main/assembly/integration-tests.xml</descriptor>
                    </descriptors>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
